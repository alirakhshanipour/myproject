<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.1/js/bootstrap.min.js" integrity="sha512-ewfXo9Gq53e1q1+WDTjaHAGZ8UvCWq0eXONhwDuIoaH8xz2r96uoAYaQCm1oQhnBfRXrvJztNXFsTloJfgbL5Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.1/css/bootstrap-grid.min.css" integrity="sha512-MJjgE48PpWFK4ZQowkReHYVkHv9Rl3ZqdxxdbisYuR0q0qRyVHSNw52YBvc0sqT0qtJFT3dirXdEMtd1l56ZUQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.1/css/bootstrap.min.css" integrity="sha512-6KY5s6UI5J7SVYuZB4S/CZMyPylqyyNZco376NM2Z8Sb8OxEdp02e1jkKk/wZxIEmjQ6DRCEBhni+gpr9c4tvA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.1/css/bootstrap-reboot.rtl.min.css" integrity="sha512-5DXXtD6Ybuub48T7V1AGFrA69qZ6+F03pHE+DT61b3c0Tf4uuyChCnxQrF7IoqSWxEtH45dYS8yDULiwlTnd0A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="/week9//HW_9//user-managment/assets//css//style.css">
    <title>user managments</title>
</head>

<body>
    <div class="container">
        <!-- login form start -->
        <div class="row justify-content-center wrapper" id="login-box">
            <div class="col-lg-10 my-auto">
                <div class="card-group myShadow rounded-3">
                    <div class="card border rounded-left p-4" style="flex-grow:1.4;">
                        <h1 class="text-center fw-bold text-primary">
                            sign into account
                        </h1>
                        <hr class="my-3">
                        <form action="#" method="post" class="px-3" id="login-form">
                            <div class="input-group input-group-lg form-group my-3">
                                <span class="input-group-text">
                                    <i class="far fa-envelope fa-lg"></i>
                                </span>
                                <input type="email" name="email" id="email" class="form-control rounded-0" placeholder="E-mail" autocomplete="off" required>
                            </div>
                            <div class="input-group input-group-lg form-group my-3">
                                <span class="input-group-text">
                                    <i class="fas fa-key fa-lg"></i>
                                </span>
                                <input type="password" name="password" id="password" class="form-control rounded-0" placeholder="Password" autocomplete="off" required>
                            </div>
                            <div class="form-group row my-3">
                                <div class="custom-control col-md-6 d-flex align-items-center justify-content-center custom-checkbox float-left">
                                    <input type="checkbox" name="rem" class="custom-control-input mx-1" id="customCheck">
                                    <label for="customCheck" class="custom-control-label">Remember Me</label>
                                </div>
                                <div class="forgot col-md-6 d-flex align-items-center justify-content-center text-capitalize">
                                    <a href="#" id="forgot-link">forgot password?</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group d-flex justify-content-center">
                                <input type="submit" value="Sign In" id="login-btn" class="btn btn-primary btn-lg btn-block w-50 myBtn">
                            </div>
                        </form>
                    </div>
                    <div class="card justify-content-center rounded-right p-4 myColor">
                        <h1 class="text-center fw-bold text-white">Hello friends</h1>
                        <hr class="my-3 bg-light myHr">
                        <p class="text-center fw-bolder text-light">enter your personal details and start your journey with us</p>
                        <button class="btn btn-outline-light btn-lg align-self-center fw-bolder mt-4 myLinkBtn lead" id="register-link">Sign Up</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- login form end -->
        <!-- Register Form Start -->
        <div class="row justify-content-center wrapper" id="register-box" style="display:none;">
            <div class="col-lg-10 my-auto">
                <div class="card-group myShadow rounded-3">
                    <div class="card justify-content-center rounded-left p-4 myColor">
                        <h1 class="text-center fw-bold text-white">Welcome Back</h1>
                        <hr class="my-3 bg-light myHr">
                        <p class="text-center fw-bolder text-light">To Keep Connected With Us Please Login With Your Personal Info</p>
                        <button class="btn btn-outline-light btn-lg align-self-center fw-bolder mt-4 myLinkBtn lead" id="login-link">Sign In</button>
                    </div>
                    <div class="card border rounded-right p-4" style="flex-grow:1.4;">
                        <h1 class="text-center fw-bold text-primary">
                            Create Account
                        </h1>
                        <hr class="my-3">
                        <form action="#" method="post" class="px-3" id="register-form">
                            <div class="input-group input-group-lg form-group my-3">
                                <span class="input-group-text">
                                    <i class="far fa-user fa-lg"></i>
                                </span>
                                <input type="text" name="name" id="name" class="form-control rounded-0" placeholder="Ful Name" autocomplete="off" required>
                            </div>
                            <div class="input-group input-group-lg form-group my-3">
                                <span class="input-group-text">
                                    <i class="far fa-envelope fa-lg"></i>
                                </span>
                                <input type="email" name="email" id="remail" class="form-control rounded-0" placeholder="E-mail" autocomplete="off" required>
                            </div>

                            <div class="input-group input-group-lg form-group my-3">
                                <span class="input-group-text">
                                    <i class="fas fa-key fa-lg"></i>
                                </span>
                                <input type="password" name="password" id="rpassword" class="form-control rounded-0" placeholder="Password" autocomplete="off" required minlength="5">
                            </div>

                            <div class="input-group input-group-lg form-group my-3">
                                <span class="input-group-text">
                                    <i class="fas fa-key fa-lg"></i>
                                </span>
                                <input type="password" name="cpassword" id="cpassword" class="form-control rounded-0" placeholder="Confirm Password" autocomplete="off" required minlength="5">
                            </div>

                            <div class="form-group d-flex justify-content-center">
                                <input type="submit" value="Sign Up" id="register-btn" class="btn btn-primary btn-lg btn-block w-50 myBtn">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Register Form End -->

        <!-- Forgot Password Form Start -->
        <div class="row justify-content-center wrapper" id="forgot-box" style="display:none;">
            <div class="col-lg-10 my-auto">
                <div class="card-group myShadow rounded-3">
                    <div class="card justify-content-center rounded-left p-4 myColor">
                        <h1 class="text-center text-capitalize fw-bold text-white">Reset Password</h1>
                        <hr class="my-3 bg-light myHr">
                        <button class="btn btn-outline-light btn-lg align-self-center fw-bolder mt-4 myLinkBtn lead" id="back-link">Back</button>
                    </div>
                    <div class="card border rounded-right p-4" style="flex-grow:1.4;">
                        <h1 class="text-center fw-bold text-primary">
                            Forgot Your Password </h1>
                        <hr class="my-3">
                        <p class="lead text-center text-secondary text-capitalize">to reset your password , enter the register email address and we will send you the rest instruction on your email</p>
                        <form action="#" method="post" class="px-3" id="forgot-form">
                            <div class="input-group input-group-lg form-group my-3">
                                <span class="input-group-text">
                                    <i class="far fa-envelope fa-lg"></i>
                                </span>
                                <input type="email" name="email" id="femail" class="form-control rounded-0" placeholder="E-mail" autocomplete="off" required>
                            </div>
                            <div class="form-group d-flex justify-content-center">
                                <input type="submit" value="Reset Password" id="forgot-btn" class="btn btn-primary btn-lg btn-block w-50 myBtn">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Forgot Password Form End -->
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/js/all.min.js" integrity="sha512-cyAbuGborsD25bhT/uz++wPqrh5cqPh1ULJz4NSpN9ktWcA6Hnh9g+CWKeNx2R0fgQt+ybRXdabSBgYXkQTTmA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.1/js/bootstrap.bundle.min.js" integrity="sha512-trzlduO3EdG7Q0xK4+A+rPbcolVt37ftugUSOvrHhLR5Yw5rsfWXcpB3CPuEBcRBCHpc+j18xtQ6YrtVPKCdsg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $(document).ready(function() {
            $("#register-link").click(function() {
                $("#login-box").hide();
                $("#register-box").show();
            });
            $("#login-link").click(function() {
                $("#register-box").hide();
                $("#login-box").show();
            });
            $("#forgot-link").click(function() {
                $("#login-box").hide();
                $("#forgot-box").show();
            });
            $("#back-link").click(function() {
                $("#forgot-box").hide();
                $("#login-box").show();
            });
        });

        //Login Ajax Request
        $('#login-btn').click(function(e) {
            if ($('#login-form')[0].checkValidity()) {
                e.preventDefault();

                $('#login-btn').val('Please Wait ...');
                $.ajax({
                    url: 'assets/php/action.php',
                    method: 'POST',
                    data: $('#login-form').serialize() + '&action=login',
                    success: function(response) {
                        console.log(response);
                    }
                })
            }
        })
    </script>
</body>

</html>